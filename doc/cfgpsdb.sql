CREATE TABLE IF NOT EXISTS `cfgpsdb`.`cfgpsdb_users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(50) NULL,
  `user_email` VARCHAR(100) NOT NULL,
  `user_pwd` VARCHAR(100) NOT NULL,
  `user_registered_date` TIMESTAMP NOT NULL DEFAULT current_timestamp,
  `user_status` INT NULL,
  `user_activationKey` VARCHAR(24) NULL,
  `user_avatar` VARCHAR(100) null,
  `user_desc` TEXT null,
  `resetpwd_key` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;
alter table `cfgpsdb_users` add unique(`user_name`, `user_email`);