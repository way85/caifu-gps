/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
	Highcharts.chart('budgetChart', {
	    chart: {
	        type: 'column'
	    },

	    title: {
	        text: 'Stacked column chart'
	    },
	    xAxis: {
	        categories: ['收入', '支出']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: '数量'
	        }
	    },
	    tooltip: {
	        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
	        shared: true
	    },
	    plotOptions: {
	        column: {
	            stacking: 'percent'
	        }
	    },
	    series: [{
	        name: '预算',
	        data: [5, 3]
	    }, {
	        name: '实际',
	        data: [2, 2]
	    }]
	});