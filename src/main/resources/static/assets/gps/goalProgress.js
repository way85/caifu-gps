/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function goalProgress() {	

	Highcharts.chart('goalProgress', {
	    chart: {
	        type: 'spline'
	    },
	    title: {
	        text: '每月目标达成进展'
	    },
	    subtitle: {
	        text: ''
	    },
	    xAxis: {
	        categories: ['1月', '2月', '3月', '4月', '5月', '6月',
	            '7月', '8月', '9月', '10月', '11月', '12月']
	    },
	    yAxis: {
	        title: {
	            text: '元'
	        },
	        labels: {
	            formatter: function () {
	                return this.value ;
	            }
	        }
	    },
	    tooltip: {
	        crosshairs: true,
	        shared: true
	    },
	    plotOptions: {
	        spline: {
	            marker: {
	                radius: 4,
	                lineColor: '#666666',
	                lineWidth: 1
	            }
	        }
	    },
	    series: [{
	        name: '计划',
	        marker: {
	            symbol: 'square'
	        },
	        data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, {
	            y: 26.5,
	            marker: {
	                symbol: 'url(https://www.highcharts.com/samples/graphics/sun.png)'
	            }
	        }, 23.3, 18.3, 13.9, 9.6]

	    }, {
	        name: '实际',
	        marker: {
	            symbol: 'diamond'
	        },
	        data: [{
	            y: 3.9,
	            marker: {
	                symbol: 'url(https://www.highcharts.com/samples/graphics/snow.png)'
	            }
	        }, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
	    }]
	});   
   
}
