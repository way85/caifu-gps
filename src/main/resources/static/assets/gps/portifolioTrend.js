/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//$(document).ready(function() {    
function portifolioTrend() {   
      
 
	Highcharts.chart('portifolioTrendChart', {

        
          
        chart: {
            zoomType: 'x'
        },
        title: {
            text: '您的账户表现 2014 - 现在'
        },
        subtitle: {
            text: document.ontouchstart === undefined ?
                    '点击并拖拽放大或缩小' :
                    'Pinch the chart to zoom in'
        },
        xAxis: {
            type: 'datetime',
            minRange: 14 * 24 * 3600000 // fourteen days
        },
        yAxis: {
            title: {
                text: '账户金额'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },

        series: [{
            type: 'area',
            name: 'USD to EUR',
            pointInterval: 24 * 3600 * 1000,
            pointStart: Date.UTC(2014, 0, 1),
            data: [
                8446, 8445, 8444, 8451,    8418, 8264,    8258, 8232,    8233, 8258,
                8283, 8278, 8256, 8292,    8239, 8239,    8245, 8265,    8261, 8269,
                8273, 8244, 8244, 8172,    8139, 8146,    8164, 8200,    8269, 8269,
                8269, 8258, 8247, 8286,    8289, 8316,    8320, 8333,    8352, 8357,
                8355, 8354, 8403, 8403,    8406, 8403,    8396, 8418,    8409, 8384,
                8386, 8372, 8390, 8400, 8389, 8400, 8423, 8423, 8435, 8422,
                8380, 8373, 8316, 8303,    8303, 8302,    8369, 8400, 8385, 8400,
                8401, 8402, 8381, 8351,    8314, 8273,    8213, 8207,    8207, 8215,
                8242, 8273, 8301, 8346,    8312, 8312,    8312, 8306,    8327, 8282,
                8240, 8255, 8256, 8273, 8209, 8151, 8149, 8213, 8273, 8273,
                8261, 8252, 8240, 8262, 8258, 8261, 8260, 8199, 8153, 8097,
                8201, 8319, 8357, 8105,    8484, 8569,    8647, 8423,    8965, 8919,
               
            ]
        }]

        });
    
}
     // })
      
