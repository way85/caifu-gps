/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function portifolio() {
	// Build the chart
    Highcharts.chart('portifolioChart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: '投资组合构成'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: '财富-GPS',
            colorByPoint: true,
            data: [{
                name: '股票基金',
                y: 56.33
            }, {
                name: '债券类基金',
                y: 24.03,
                sliced: true,
                selected: true
            }, {
                name: '其他',
                y: 0.2
            }]
        }]
    });
}


