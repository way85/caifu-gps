
package com.caifugps.ctrl;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import org.springframework.web.servlet.ModelAndView;

import org.springframework.ui.Model;

import com.caifugps.model.User;

/**
 * @Date 12/19/2016
 * @author Ray Yu
 * Updated for login/logout and index for not logged user with static web page
 */
@Controller
public class StaticPagesCtrl {
	
	@ModelAttribute("displayname")
	public String populateDisplayname() {
		String displayname = "未登录用户";
		return displayname; // populates form for the first time if its null
	}

	@ModelAttribute("id")
	public int populateId() {
		int id = 0;
		return id; // populates form for the first time if its null
	}

	@GetMapping(value = { "/", "/index" })
	public String indexRoot(Model model) {
		return "index";
	}
	
	@GetMapping(value = "/logout")
	public String logout(@ModelAttribute("displayname") String displayname, SessionStatus sessionStatus, Model model) {

		if (displayname != "未登录用户") {
			sessionStatus.setComplete();// 将所有的会话注销
		}

		model.addAttribute("displayname", "未登录用户");		
		return "index";
	}
	
	//TODO add static page for userAcc/setting/security to reset the password
	@GetMapping(value="userAcc/setting/security")
	public String settingSecurity(HttpSession session){
		return "userAcc/setting/security";
	}
	
	//TODO add static page for userAcc/setting/security to reset the userInfo
	@GetMapping(value="/setting/info")
	public String settingInfoPage(HttpSession session){		
		return "userAcc/setting/info";
	}

	
	
	@RequestMapping("/404")
	public ModelAndView pageNotFound() {
		return new ModelAndView("404");
	}
	
	@RequestMapping("/500")
	public ModelAndView error500() {
		return new ModelAndView("500");
	}
	
	 @RequestMapping(value = "/newUser", method = RequestMethod.GET)
	    public ModelAndView newUser() {
	        return new ModelAndView("newUser", "command", new User());
	    }

	    @RequestMapping(value = "/validateUser", method = RequestMethod.GET)
	    public ModelAndView validateUser() {
	        return new ModelAndView("login", "command", new User());
	    }

}
