/*
// * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caifugps.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.ui.Model;

import com.caifugps.model.User;
import com.caifugps.model.Cam;
import com.caifugps.model.Goal;
import com.caifugps.model.PorPer;
import com.caifugps.repo.UserRepo;
import com.caifugps.repo.GoalRepo;
import com.caifugps.repo.CamRepo;
import com.caifugps.repo.PorPerRepo;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
@Controller
@SessionAttributes({ "id", "displayname" })
public class UserCtrl {

	@Autowired
	UserRepo userRepo;
	@Autowired
	GoalRepo goalRepo;
	@Autowired
	CamRepo camRepo;
	@Autowired
	PorPerRepo porPerRepo;

	@RequestMapping(value = "/validateUser", method = RequestMethod.POST)
	public String validateUser(@ModelAttribute("simpleQ") User user, Model model) {

		String displayname = user.getDisplayname();
		String password = user.getPassword();
		Boolean validateresult = userRepo.validateUser(displayname, password);
		String returnStr = "";

		System.out.println("this is the test for user controller for displayname    " + displayname);

		ArrayList<String> alertmessages = new ArrayList();

		if (validateresult == false) {
			alertmessages.add("用户名或者密码错误");
			model.addAttribute("alertmessages", alertmessages);
			returnStr = "login";
		} else {

			int id = userRepo.findId(displayname, password);
			String status = "";

			Cam cam = new Cam();
			Goal goal = new Goal();
			
			cam = camRepo.findOne(id);

			if (cam.getCamid() == null) {
				status = "new";
				alertmessages.add("您还未建立财富目标，请先建立财富目标。");
				model.addAttribute("alertmessages", alertmessages);
				model.addAttribute("displayname", displayname);
				model.addAttribute("id", id);
				model.addAttribute("status", status);
				returnStr = "homepage";
			} else {
				goal = goalRepo.findOne(id);
				status = "existed";
				String totalamount = "";
				String ppdate = "";

				List<String> totalamountlist = new ArrayList();
				List<String> ppdatelist = new ArrayList();

				totalamountlist = porPerRepo.findById(id, "totalamount");
				ppdatelist = porPerRepo.findById(id, "ppdate");

				String currentamount = totalamountlist.get(0);
				model.addAttribute("displayname", displayname);
				model.addAttribute("id", id);
				model.addAttribute("cam", cam);
				model.addAttribute("totalamountlist", totalamountlist);
				model.addAttribute("ppdatelist", ppdatelist);
				model.addAttribute("currentamount", currentamount);
				model.addAttribute("goal", goal);
				model.addAttribute("alertmessages", alertmessages);
				model.addAttribute("status", status);
				returnStr = "homepage";
			}
		}
		return returnStr;
	}

	@RequestMapping(value = "/homepage", method = RequestMethod.GET)
	public String homepage(@ModelAttribute("displayname") String displayname, @ModelAttribute("id") int id,
			Model model) {

		if (displayname.equals("未登录用户")) {
			return "validateUserForm";
		} else {
			Cam cam = new Cam();
			Goal goal = new Goal();
			

			cam = camRepo.findOne(id);
			if (cam.getAsofdate() == null) {
				return "newGoalForm";
			} else {
				goal = goalRepo.findOne(id);
				ArrayList<String> alertmessages = new ArrayList();
				String totalamount = "";
				String ppdate = "";

				List<String> totalamountlist = porPerRepo.findById(id, "totalamount");
				List<Date> ppdatelist = porPerRepo.findById(id, "ppdate");

				String currentamount = totalamountlist.get(0);

				model.addAttribute("displayname", displayname);
				model.addAttribute("id", id);
				model.addAttribute("cam", cam);
				model.addAttribute("totalamountlist", totalamountlist);
				model.addAttribute("ppdatelist", ppdatelist);
				model.addAttribute("currentamount", currentamount);
				model.addAttribute("goal", goal);
				model.addAttribute("alertmessages", alertmessages);
				return "homepage";
			}
		}
	}

	@RequestMapping(value = "/newUser", method = RequestMethod.POST)
	public String newUser(@ModelAttribute("user") User user, Model model) {

		ArrayList<String> alertmessages = new ArrayList();
		user.setUserregistered(new Date());

		alertmessages.add("请先进行您的个人情况， 风险偏好，财富状况定位。");
		alertmessages.add("在进行财富状况定位后，设定您的财富目标从而开始您的财富旅程。");
		alertmessages.add("让财富 - GPS陪伴您的财富旅程，达成您的财富目标。");

		int id = userRepo.save(user);
		model.addAttribute("id", id);
		model.addAttribute("displayname", user.getDisplayname());
		model.addAttribute("status", "new");
		model.addAttribute("alertmessages", alertmessages);
		return "homepage";

	}

}
