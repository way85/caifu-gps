/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caifugps.repo;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.HibernateException;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Transactional
@Repository
public class FinIntPerRepo {

	SessionFactory sessionFactory;

	public FinIntPerRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	public String findByFinancialintrumentperformance(String financialintrumentid,
			Date financialintrumentperformancedate) {

		Date cdate = new Date();
		String finintper = "";

		try {

			String hql = "SELECT financialintrumentperformance FROM Financialintrumentperformance WHERE financialintrumentid = "
					+ "'" + financialintrumentid + "'" + " and financialintrumentperformancedate= " + "'" + cdate + "'";

			finintper = currentSession().createQuery(hql).uniqueResult().toString();

		} catch (HibernateException ex) {

			ex.printStackTrace();
		}

		return finintper;
	}

}
