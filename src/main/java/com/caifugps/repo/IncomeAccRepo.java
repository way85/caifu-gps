package com.caifugps.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.caifugps.model.Account;
import com.caifugps.model.IncomeAcc;
import com.caifugps.model.OutcomeAcc;

@Transactional
@Repository
public class IncomeAccRepo {

	SessionFactory sessionFactory;

	public IncomeAccRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	public String save(IncomeAcc account) {

		String returnStr = "";

		try {
			sessionFactory.getCurrentSession().save(account);
			returnStr = "success";
		} catch (Exception ex) {
			returnStr = "fail";
		}

		return returnStr;
	}

	public List<IncomeAcc> findAll(int id, String month ) {

		List<IncomeAcc> incomeAccList = currentSession().createCriteria(IncomeAcc.class)
				.add(Restrictions.eq( "id", id))
				.add(Restrictions.eq( "month", month))
				.list();
		return incomeAccList;
	}

	public long count(int id, String date, String month) {
		long size = currentSession().createCriteria(IncomeAcc.class)
				.add(Restrictions.eq( "id", id))
				.add(Restrictions.eq( "month", month))	
				.list().size();
		return size;
	}



}
