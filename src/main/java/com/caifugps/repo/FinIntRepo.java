/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caifugps.repo;

import com.caifugps.model.FinInt;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.hibernate.HibernateException;

import java.util.List;

import javax.transaction.Transactional;

/**
 *
 * @author Administrator
 */

@Transactional
@Repository
public class FinIntRepo {

	SessionFactory sessionFactory;

	public FinIntRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public List<FinInt> findByType(String financialintrumenttype) {

		List<FinInt> finintList = null;

		try {

			String hql = "FROM FinInt WHERE financialintrumenttype = " + "'" + financialintrumenttype + "'";

			finintList = (List<FinInt>)currentSession().createQuery(hql).list();

		} catch (HibernateException ex) {

			ex.printStackTrace();
		}
		return finintList;
	}

}
