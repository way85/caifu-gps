package com.caifugps.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.caifugps.model.FinalPro;
import com.caifugps.model.UserPro;

@Transactional
@Repository
public class UserProRepo {

	SessionFactory sessionFactory;

	public UserProRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	public List<UserPro> findAll() {

		List<UserPro> userProList = currentSession().createCriteria(UserPro.class).list();

		return userProList;
	}

	public long count() {
		long size = findAll().size();
		return size;
	}

	public UserPro findOne(String displayname) {

		UserPro userPro = (UserPro) currentSession().get(UserPro.class, displayname);
		return userPro;

	}

	
	public UserPro findOne(int id) {

		String userProHql = "from UserPro where id =" + "'" + id + "'";
		Query query=currentSession().createQuery(userProHql);
		UserPro userPro= (UserPro)query.list().get(0);
		return userPro;

	}

	

	public String findId(String displayName) {
		String id = "";

		String idHql = "SELECT id FROM User WHERE displayname = " + "'" + displayName + "'";
		id = currentSession().createQuery(idHql).uniqueResult().toString();
		return id;
	}

	public int save(UserPro userPro) {

		int id = 0;
		
			System.out.println("this is a test for UserRepo save uer");
			id = (Integer)currentSession().save(userPro);			
		
		return id;

	}
}
