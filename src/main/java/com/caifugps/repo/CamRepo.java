/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caifugps.repo;

import com.caifugps.model.Cam;
import com.caifugps.model.Goal;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.hibernate.Query;
import org.hibernate.Session;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

/**
 *
 * @author Administrator
 */

@Transactional
@Repository
public class CamRepo {

	SessionFactory sessionFactory;

	public CamRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	public List<Cam> findAll() {

		List<Cam> camList = currentSession().createCriteria(Cam.class).list();
		return camList;
	}

	public Cam findOne(int id) {

		Cam cam = new Cam();
		
		String camHql = "from Cam where id =" + "'" + id + "'";
		Query query=currentSession().createQuery(camHql);
		if (query.list().size()>1){
		cam = (Cam)query.list().get(0);
		}
		return cam;

	}

	public String save(Cam cam, int id) {

		Integer camid = 0;
		Date date = new Date();

		cam.setId(id);
		cam.setAsofdate(date);

		try {

			cam.setAsofdate(date);
			camid = (Integer) currentSession().save(cam);
			return "success";
		} catch (Exception ex) {
			return "fail";
		}

	}

}
