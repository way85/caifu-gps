/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caifugps.repo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.caifugps.model.PorPer;

/**
 *
 * @author Administrator
 */
@Transactional
@Repository

public class PorPerRepo {

	SessionFactory sessionFactory;

	public PorPerRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void save(PorPer portifolioperformance) {
		int ppid = 0;

		Date date = new Date();

		portifolioperformance.setPpdate(date);

	
		ppid = (Integer) currentSession().save(portifolioperformance);

	}

	public List findById(int id, String columnname) {

		List<PorPer> resultarraylist = new ArrayList<PorPer>();

		String portifolioPerformanceHql = "Select " + columnname + " FROM PorPer WHERE id = " + "'" + id
				+ "'" + "order by ppdate desc";
		
		resultarraylist = currentSession().createQuery(portifolioPerformanceHql).list();

		return resultarraylist;
	}

}
