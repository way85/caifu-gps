/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caifugps.repo;

import com.caifugps.model.ScorePro;
import com.caifugps.model.FinalPro;
import com.caifugps.model.Goal;
import com.caifugps.service.ScoreService;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 *
 * @author Administrator
 */
@Transactional
@Repository
public class ScoreRepo {

	SessionFactory sessionFactory;

	public ScoreRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	public FinalPro save(ScorePro scorePro, int id) {

		ScoreService scoreService = new ScoreService();
		FinalPro finalPro = scoreService.scoreCalculation(scorePro);

		Date asofdate = new Date();

		finalPro.setId(id);
		scorePro.setId(id);
		scorePro.setAsofdate(asofdate);

		int spid = (Integer) currentSession().save(scorePro);
		int fpid = (Integer) currentSession().save(finalPro);

		return finalPro;
	}

	public void save(ScorePro scorePro) {

		ScoreService scoreService = new ScoreService();
		FinalPro finalPro = scoreService.scoreCalculation(scorePro);

		Date asofdate = new Date();

		finalPro.setId(scorePro.getId());

		scorePro.setAsofdate(asofdate);

		int spid = (Integer) currentSession().save(scorePro);
		finalPro.setFpid(scorePro.getSpid());

		currentSession().save(finalPro);
	}

	public ScorePro findOne(int id) {

		ScorePro scorePro = (ScorePro) currentSession().get(ScorePro.class, id);
		return scorePro;

	}

}
