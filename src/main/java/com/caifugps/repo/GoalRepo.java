package com.caifugps.repo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.caifugps.model.Goal;
import com.caifugps.service.GoalService;

@Transactional
@Repository
public class GoalRepo {

	SessionFactory sessionFactory;

	public GoalRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	
	
	public Goal findOne(int id) {

		String goalHql = "from Goal where id =" + "'" + id + "'";
		Query query=currentSession().createQuery(goalHql);
		Goal goal = (Goal)query.list().get(0);
		return goal;

	}

	public Goal save(Goal goal, int id) {

		Integer goalid = 0;
		String timeframescore = "";

		GoalService goalService = new GoalService();
		timeframescore = String.valueOf(goalService.timeframescoreCalculation(goal.getTimeframe()));
		Date date = new Date();

		goal.setId(id);
		goal.setGoalstatus("进行中");
		goal.setAsofdate(date);

		System.out.println("this is the test for goal type" + goal.getGoaltype());

		goalid = (Integer) currentSession().save(goal);

		return goal;
	}

	public void save(Goal goal) {

		Date date = new Date();

		goal.setGoalstatus("进行中");
		goal.setAsofdate(date);

		int goalid = (Integer) currentSession().save(goal);

	}

}
