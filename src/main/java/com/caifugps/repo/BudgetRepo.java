package com.caifugps.repo;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.caifugps.model.Budget;

@Transactional
@Repository
public class BudgetRepo {

	SessionFactory sessionFactory;

	public BudgetRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	public Budget findOne(int id) {

		Budget budget = (Budget) currentSession().get(Budget.class, id);
		return budget;

	}

	public String save(Budget budget) {

		try{
		int budgetid=(Integer) currentSession().save(budget);
			return "success";
		} catch (Exception ex) {
			return "fail";
		}

	}
}
