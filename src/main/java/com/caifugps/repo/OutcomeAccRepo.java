package com.caifugps.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.caifugps.model.Account;
import com.caifugps.model.OutcomeAcc;

@Transactional
@Repository
public class OutcomeAccRepo {

	SessionFactory sessionFactory;

	public OutcomeAccRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	public String save(OutcomeAcc account) {

		String returnStr = "";

		try {
			sessionFactory.getCurrentSession().save(account);
			returnStr = "success";
		} catch (Exception ex) {
			returnStr = "fail";
		}

		return returnStr;
	}

	public List<OutcomeAcc> findAll(int id, String month) {

		List<OutcomeAcc> outcomeAccList = currentSession().createCriteria(OutcomeAcc.class)
				.add(Restrictions.eq( "id", id))
				.add(Restrictions.eq( "month", month))
				.list();
		return outcomeAccList;
	}

	public long count(int id,String month) {
		long size = currentSession().createCriteria(OutcomeAcc.class)
				.add(Restrictions.eq( "id", id))
				.add(Restrictions.eq( "month", month))
				.list().size();
		return size;
	}

}
