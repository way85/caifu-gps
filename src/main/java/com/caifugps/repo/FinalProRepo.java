/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caifugps.repo;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.caifugps.model.FinalPro;
import com.caifugps.model.Goal;
import com.caifugps.model.UserPro;

/**
 *
 * @author Administrator
 */
@Transactional
@Repository
public class FinalProRepo {

	SessionFactory sessionFactory;

	public FinalProRepo(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session currentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public FinalPro findOne(int id) {

		String finalProHql = "from FinalPro where id =" + "'" + id + "'";
		Query query=currentSession().createQuery(finalProHql);
		FinalPro finalpro = (FinalPro)query.list().get(0);
		return finalpro;

	}

	

}
