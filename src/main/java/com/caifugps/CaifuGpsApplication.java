package com.caifugps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaifuGpsApplication {

	public static void main(String[] args) {	
				
		SpringApplication.run(CaifuGpsApplication.class, args);
	}
}
