/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caifugps.service;

import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */

@Service
public class GoalService {

	public int timeframescoreCalculation(String timeframe) {

		int timeframeInt = Integer.parseInt(timeframe);

		int timeframescore = 0;

		if (15 > timeframeInt && timeframeInt >= 4) {
			switch (timeframeInt) {
			case 4:
				timeframescore = 63;
			case 5:
				timeframescore = 64;
			case 6:
				timeframescore = 65;
			case 7:
				timeframescore = 74;
			case 8:
				timeframescore = 74;
			case 9:
				timeframescore = 74;
			case 10:
				timeframescore = 74;
			case 11:
				timeframescore = 74;

			case 12:
				timeframescore = 74;
			case 13:
				timeframescore = 74;
			case 14:
				timeframescore = 74;
			}
		} else if (timeframeInt >= 15) {
			timeframescore = 78;
		} else {
			timeframescore = 0;
		}
		return timeframescore;
	}

	public int finalScoreGoalCalculation(String timeframescore, String riskscore, String financialscore) {
		// calculation for timeframescore
		int timeframescoreInt = Integer.parseInt(timeframescore);
		int riskscoreInt = Integer.parseInt(riskscore);
		int financialscoreInt = Integer.parseInt(financialscore);

		int finalscoregoal = (int) (timeframescoreInt - 0.2 * (100 - financialscoreInt) - 0.3 * (100 - riskscoreInt));

		if (finalscoregoal > 0) {
			finalscoregoal = finalscoregoal;
		} else {
			finalscoregoal = 0;
		}
		System.out.println("this is the test for goalservice finalscoregoal  " + finalscoregoal);
		return finalscoregoal;
	}

}
