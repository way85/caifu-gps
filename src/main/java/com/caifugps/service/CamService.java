/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caifugps.service;

import com.caifugps.model.Cam;

import java.util.Date;

import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */

@Service
public class CamService {

	public Cam camculation(int finalscoregoal) {

		int finalscore = finalscoregoal;
		int camnumber = 0;
		Cam cam = new Cam();

		if (finalscore < 0) {
			camnumber = 1; // conservative equity is 20%
		} else if (30 > finalscore && finalscore >= 0) {
			camnumber = 2; // moderate with income equity is 30%
		} else if (36 > finalscore && finalscore >= 30) {
			camnumber = 3; // moderate equity is 40%
		} else if (41 > finalscore && finalscore >= 36) {
			camnumber = 4; // balance equity is 50%
		} else if (58 > finalscore && finalscore >= 41) {
			camnumber = 5; // growth with income equity is 60%
		} else if (69 > finalscore && finalscore >= 58) {
			camnumber = 6; // growth with income equity is 70%
		} else if (finalscore >= 69) {
			camnumber = 7; // growth with income equity is 85%
		}

		// calculation for score

		if (camnumber == 1) {
			cam.setStockportion("20");
			cam.setBondsportion("80");
			cam.setCamtype("保守型");
		} else if (camnumber == 2) {
			cam.setStockportion("30");
			cam.setBondsportion("70");
			cam.setCamtype("收入稳健型");

		} else if (camnumber == 3) {
			cam.setStockportion("40");
			cam.setBondsportion("60");
			cam.setCamtype("稳健型");

		} else if (camnumber == 4) {
			cam.setStockportion("50");
			cam.setBondsportion("50");
			cam.setCamtype("平衡型");

		} else if (camnumber == 5) {
			cam.setStockportion("60");
			cam.setBondsportion("40");
			cam.setCamtype("收入成长型");

		} else if (camnumber == 6) {
			cam.setStockportion("70");
			cam.setBondsportion("30");
			cam.setCamtype("成长型");

		} else if (camnumber == 7) {
			cam.setStockportion("85");
			cam.setBondsportion("15");
			cam.setCamtype("进取成长型");
		}
		cam.setAsofdate(new Date());
		return cam;
	}

}
