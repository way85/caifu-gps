/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caifugps.service;

import com.caifugps.model.ScorePro;
import com.caifugps.model.FinalPro;

import java.util.Date;

import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */

@Service
public class ScoreService {

	public FinalPro scoreCalculation(ScorePro scorePro) {

		int currentage = Integer.parseInt((String) scorePro.getCurrentage());

		int investmentknowledge = Integer.parseInt(String.valueOf(scorePro.getInvestmentknowledge()));
		int investmentexperience = Integer.parseInt(String.valueOf(scorePro.getInvestmentexperience()));
		int selfscore = Integer.parseInt(String.valueOf(scorePro.getSelfscore()));
		int bailout = Integer.parseInt(String.valueOf(scorePro.getBailout()));
		int emergencyfund = Integer.parseInt(String.valueOf(scorePro.getEmergencyfund()));
		int annualincome = Integer.parseInt(String.valueOf(scorePro.getAnnualincome()));
		int essentialexpense = Integer.parseInt(String.valueOf(scorePro.getEssentialexpense()));
		int overallsituation = Integer.parseInt(String.valueOf(scorePro.getOverallsituation()));

		int personalscore = 0;
		int riskscore = 0;
		int financialscore = 0;
		int finalscore = 0;

		Date asofdate = new Date();
		int version = 1;

		switch (investmentknowledge) {
		case 1:
			// Statements
			riskscore = 0;
			break;
		case 2:

			riskscore = 5;
			break;
		case 3:
			riskscore = 11;
		}
		switch (investmentexperience) {
		case 1:
			riskscore = riskscore + 0;
			break;
		case 2:
			riskscore = riskscore + 9;
			break;
		case 3:
			riskscore = riskscore + 12;
		}
		switch (bailout) {
		case 1:
			riskscore = riskscore + 31;
			break;
		case 2:
			riskscore = riskscore + 25;
			break;
		case 3:
			riskscore = riskscore + 17;
			break;
		case 4:
			riskscore = riskscore + 0;
		}
		switch (selfscore) {
		case 1:
			riskscore = riskscore - 64;
			break;
		case 2:
			riskscore = riskscore - 13;
			break;
		case 3:
			riskscore = riskscore + 22;
			break;
		case 4:
			riskscore = riskscore + 31;
			break;
		case 5:
			riskscore = riskscore + 56;
		}
		// The following is to calculate the financiall score
		switch (emergencyfund) {
		case 1:
			financialscore = 0;
			break;
		case 2:
			financialscore = 13;
			break;
		case 3:
			financialscore = 18;
			break;
		case 4:
			financialscore = 27;

		}
		switch (annualincome) {
		case 1:
			financialscore = financialscore + 0;
			break;
		case 2:
			financialscore = financialscore + 14;
			break;
		case 3:
			financialscore = financialscore + 18;
			break;
		case 4:
			financialscore = financialscore + 20;
			break;
		case 5:
			financialscore = financialscore + 25;
			break;
		case 6:
			financialscore = financialscore + 26;
			break;
		case 7:
			financialscore = financialscore + 28;

		}
		switch (essentialexpense) {
		case 1:
			financialscore = financialscore + 20;
			break;
		case 2:
			financialscore = financialscore + 18;
			break;
		case 3:
			financialscore = financialscore + 15;
			break;
		case 4:
			financialscore = financialscore + 13;
			break;
		case 5:
			financialscore = financialscore + 0;
		}
		switch (overallsituation) {
		case 1:
			financialscore = financialscore + 30;
			break;
		case 2:
			financialscore = financialscore + 25;
			break;
		case 3:
			financialscore = financialscore + 15;
			break;
		case 4:
			financialscore = financialscore + 10;

		}
		switch (currentage) {
		case 1:
			personalscore = personalscore + 10;
			break;
		case 2:
			personalscore = personalscore + 25;
			break;
		case 3:
			personalscore = personalscore + 20;
			break;
		case 4:
			personalscore = personalscore + 15;
			break;
		case 5:
			personalscore = personalscore + 0;

		}

		finalscore = riskscore + financialscore + personalscore;

		FinalPro finalPro = new FinalPro();
		finalPro.setAsofdate(asofdate);
		finalPro.setFinalscore(finalscore + "");
		finalPro.setFinancialscore(financialscore + "");
		finalPro.setRiskscore(riskscore + "");
		finalPro.setPersonalscore(personalscore + "");
		finalPro.setVersion((char) (version + '0'));

		return finalPro;
	}

}
