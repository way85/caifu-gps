package com.caifugps.restful;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.caifugps.model.Goal;

import com.caifugps.repo.GoalRepo;

/**
 *
 * @author Administrator
 */

@RestController
public class GoalRestCtrl {

	@Autowired
	GoalRepo goalRepo;

	@PostMapping(value = "/newGoal")
	public void newGoal(@RequestBody Goal goal) {

		goalRepo.save(goal);
	}

	@GetMapping(value = "/getGoal/{id}")
	public Goal getGoal(@PathVariable int id) {

		Goal goal = goalRepo.findOne(id);

		return goal;
	}

}
