/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.caifugps.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.caifugps.model.FinalPro;
import com.caifugps.model.Goal;
import com.caifugps.model.ScorePro;

import com.caifugps.repo.ScoreRepo;

/**
 *
 * @author Administrator
 * 
 * update in Match 27 to delete old newscore method
 */
@RestController 
@SessionAttributes({"id", "displayname"})
public class ScoreRestCtrl {  
	
	@Autowired
	ScoreRepo scoreRepo;

	
	@GetMapping(value = "/getScore/{id}")
	public ScorePro getScorePro(@PathVariable int id) {
		ScorePro scorePro = scoreRepo.findOne(id);
		return scorePro;
	}

	@PostMapping(value = "/newScore")
	public void newScore(@RequestBody ScorePro scorePro) {			
		scoreRepo.save(scorePro);
	}
}
