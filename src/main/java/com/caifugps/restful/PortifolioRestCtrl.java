package com.caifugps.restful;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.caifugps.model.Cam;
import com.caifugps.model.FinalPro;
import com.caifugps.model.Goal;
import com.caifugps.model.FinInt;
import com.caifugps.model.PorPer;
import com.caifugps.repo.CamRepo;
import com.caifugps.repo.FinIntRepo;
import com.caifugps.repo.FinalProRepo;
import com.caifugps.repo.GoalRepo;
import com.caifugps.repo.PorPerRepo;
import com.caifugps.service.CamService;
import com.caifugps.service.GoalService;


/**
 *
 * @author Administrator
 */

@RestController
public class PortifolioRestCtrl {
	
	@Autowired
	GoalRepo goalRepo;
	@Autowired
	FinalProRepo finalProRepo;
	@Autowired
	PorPerRepo porPerRepo;
	@Autowired
	CamRepo camRepo;
	@Autowired
	FinIntRepo finIntRepo;
	
	@Autowired
	GoalService goalService;	
	@Autowired
	CamService camService;
	
	@GetMapping(value = "/getNewPortifolio/{id}")
	public HashMap<String, Object> getNewPortifolio(@PathVariable int id) {
		
		HashMap<String,Object> returnHm = new HashMap<String,Object>();
		
		DecimalFormat decimalFormat = new DecimalFormat("#0.00");

		Goal goal = goalRepo.findOne(id);
		FinalPro finalPro=finalProRepo.findOne(id);
		
		String riskscore = finalPro.getRiskscore();
		String financialscore = finalPro.getFinancialscore();
		String timeframe = goal.getTimeframe();
		String timeframescore = String.valueOf(goalService.timeframescoreCalculation(timeframe));
		String finalscoregoal =goalService.finalScoreGoalCalculation(timeframescore, riskscore, financialscore) + "";
	
		Cam cam = camService.camculation(Integer.parseInt(finalscoregoal));
		camRepo.save(cam, id);
				
		List<FinInt> stocks = finIntRepo.findByType("股票型基金");
		List<FinInt> bonds = finIntRepo.findByType("纯债型基金");
		
		returnHm.put("cam", cam);
		returnHm.put("stocks", stocks);
		returnHm.put("bonds", bonds);
		return returnHm;
		
	}
	
	//Only for stockIntrumentId and bondIntrumentId, will add fixincome and shortSaving and so on
	@PostMapping(value = "/saveNewPortifolio/{id}") 
//	public HashMap<String, Object> saveNewPortifolio(@PathVariable int id, @RequestBody PorPer porPer) {
	public PorPer saveNewPortifolio(@PathVariable int id, @RequestBody PorPer porPer) {
		
//		HashMap<String, Object> returnHm =new HashMap<String, Object> ();
		
		DecimalFormat decimalFormat = new DecimalFormat("#0.00");

		Goal goal = goalRepo.findOne(id);
		Cam cam = camRepo.findOne(id);
		
		int stockportion = Integer.parseInt(cam.getStockportion());
		int bondsportion = Integer.parseInt(cam.getBondsportion());
		double totalamount = Double.valueOf(goal.getStartamount());
		double stockamount = totalamount * stockportion / 100;
		double bondsamount = totalamount * bondsportion / 100;
		double currentamount = totalamount;
		
		porPer.setId(id);
		porPer.setTotalamount(decimalFormat.format(totalamount));
		porPer.setStockamount(decimalFormat.format(stockamount));
		porPer.setBondsamount(decimalFormat.format(bondsamount));
		porPerRepo.save(porPer);
		
//		returnHm.put("cam", cam);
//		returnHm.put("currentamount", currentamount);
//		returnHm.put("goal", goal);
//		returnHm.put("alertmessages", alertmessages);
//		returnHm.put("status", status);
//		
//		ArrayList<String> alertmessages = new ArrayList();
//		alertmessages.add("由于您刚刚开始您的财富旅程，您的数据还不足够分析。随着时间的前进您的财富导航将提供给您更多重要信息，请持续关注。");
//		alertmessages.add("您已完成了财富定位，目标设定并生成了第一个投资组合。请持续关注财富导航并开始您的财富旅程。");
//
//		return returnHm;
		return porPer;
	
				
	}


}
