package com.caifugps.restful;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.caifugps.model.Cam;
import com.caifugps.model.Goal;
import com.caifugps.repo.CamRepo;
import com.caifugps.repo.GoalRepo;
import com.caifugps.repo.PorPerRepo;
import com.caifugps.repo.UserRepo;

import java.util.HashMap;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */

@RestController
public class DashboardRestCtrl {

	@Autowired
	UserRepo userRepo;
	@Autowired
	GoalRepo goalRepo;
	@Autowired
	CamRepo camRepo;
	@Autowired
	PorPerRepo porPerRepo;
	

	@GetMapping(value = "/getDashboard/{id}")
	public HashMap<String, Object> getDashboard(@PathVariable int id) {

		HashMap<String, Object> returnHm =new HashMap<String, Object> ();
		ArrayList<String> alertmessages = new ArrayList();
		
		Cam cam = new Cam();
		Goal goal = new Goal();
		goal = goalRepo.findOne(id);
		cam = camRepo.findOne(id);		
		
		String status = "existed";
		String totalamount = "";
		String ppdate = "";

		ArrayList<String> totalamountlist = new ArrayList();
		ArrayList<String> ppdatelist = new ArrayList();

		totalamountlist = (ArrayList<String>) porPerRepo.findById(id, "totalamount");
		ppdatelist = (ArrayList<String>) porPerRepo.findById(id, "ppdate");

		String currentamount = totalamountlist.get(0);
		
		returnHm.put( "cam", cam);
		returnHm.put("totalamountlist", totalamountlist);
		returnHm.put("ppdatelist", ppdatelist);
		returnHm.put("currentamount", currentamount);
		returnHm.put("goal", goal);
		returnHm.put("alertmessages", alertmessages);
		returnHm.put("status", status);

		return returnHm;
	}

}
