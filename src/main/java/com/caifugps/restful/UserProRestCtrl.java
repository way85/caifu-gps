package com.caifugps.restful;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.caifugps.model.UserPro;

import com.caifugps.repo.UserProRepo;

/**
 *
 * @author Administrator
 */

@RestController
public class UserProRestCtrl {

	@Autowired
	UserProRepo userProRepo;
	
	

	@PostMapping(value = "/newUserPro")
	public void newUserPro(@RequestBody UserPro userPro) {

		userProRepo.save(userPro);
	}

	@GetMapping(value = "/getUserPro/{id}")
	public UserPro getUserPro(@PathVariable int id) {

		UserPro userPro = userProRepo.findOne(id);

		return userPro;
	}

}
