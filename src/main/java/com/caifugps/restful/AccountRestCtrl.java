package com.caifugps.restful;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.caifugps.model.IncomeAcc;
import com.caifugps.model.OutcomeAcc;

import com.caifugps.repo.IncomeAccRepo;
import com.caifugps.repo.OutcomeAccRepo;

/**
 *
 * @author Administrator
 */

@RestController
public class AccountRestCtrl {

	@Autowired
	IncomeAccRepo incomeAccRepo;

	@Autowired
	OutcomeAccRepo outcomeAccRepo;

	@PostMapping(value = "/newIncomeAcc")
	public void newIncomeAcc(@RequestBody IncomeAcc incomeAcc) throws ParseException {
				
		String dateStr = incomeAcc.getDate();  
		String year=dateStr.substring(0,4);
		String month=dateStr.substring(5,7);
		String date=dateStr.substring(8,10);
		
		incomeAcc.setMonth(month);
		incomeAcc.setYear(year);		
		
		System.out.println(dateStr+" --- "+year+" --- "+month+" --- "+date);
		
	    incomeAccRepo.save(incomeAcc);
	}

	@PostMapping(value = "/newOutcomeAcc")
	public void newOutcomeAcc(@RequestBody OutcomeAcc outcomeAcc) {
		String dateStr = outcomeAcc.getDate();  
		String year=dateStr.substring(0,4);
		String month=dateStr.substring(5,7);
		String date=dateStr.substring(8,10);
		
		outcomeAcc.setMonth(month);
		outcomeAcc.setYear(year);		
		
		System.out.println(dateStr+" --- "+year+" --- "+month+" --- "+date);
		
	    outcomeAccRepo.save(outcomeAcc);
	}

	@GetMapping(value = "/getIncomeAccList/{id}/{month}/")
	public List<IncomeAcc> getIncomeAccList(@PathVariable int id, @PathVariable String month) {

		List<IncomeAcc> incomeAccList = incomeAccRepo.findAll(id, month);
		return incomeAccList;
	}
	
	@GetMapping(value = "/getOutcomeAccList/{id}/{month}/")
	public List<OutcomeAcc> getOutcomeAccList(@PathVariable int id, @PathVariable String month) {

		List<OutcomeAcc> outcomeAccList = outcomeAccRepo.findAll(id, month);
		return outcomeAccList;
	}

}
