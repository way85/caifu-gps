package com.caifugps.restful;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.caifugps.model.Budget;

import com.caifugps.repo.BudgetRepo;

/**
 *
 * @author Administrator
 */

@RestController
public class BudgetRestCtrl {

	@Autowired
	BudgetRepo budgetRepo;
	
	

	@PostMapping(value = "/newBudget")
	public void newBudget(@RequestBody Budget budget) {

		budgetRepo.save(budget);
	}

	@GetMapping(value = "/getBudget/{id}")
	public Budget getBudget(@PathVariable int id) {

		Budget budget = budgetRepo.findOne(id);

		return budget;
	}

}
