package com.caifugps.model;

import java.util.Date;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Oct 16 2016 Entity Records
 */

@Entity
@Table(name = "records")
public class Records {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "recordtid", nullable = false)
	private Integer recordid;

	@NotNull
	@Column(name = "asofdate")
	private Date asofdate;

	@NotNull
	@Column(name = "type")
	private String type;

	@NotNull
	@Column(name = "inOrOut")
	private String inOrOut;

	@Column(name = "desc")
	private String desc;

	@NotNull
	@Column(name = "amount")
	private BigDecimal amount;

	@NotNull
	@Column(name = "id")
	private int id;

	public Records() {

	}

	public Records(Integer recordid, Date asofdate, String type, String inOrOut, String desc, BigDecimal amount,
			int id) {
		super();
		this.recordid = recordid;
		this.asofdate = asofdate;
		this.type = type;
		this.inOrOut = inOrOut;
		this.desc = desc;
		this.amount = amount;
		this.id = id;
	}

	public Integer getRecordid() {
		return recordid;
	}

	public void setRecordid(Integer recordid) {
		this.recordid = recordid;
	}

	public Date getAsofdate() {
		return asofdate;
	}

	public void setAsofdate(Date asofdate) {
		this.asofdate = asofdate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getInOrOut() {
		return inOrOut;
	}

	public void setInOrOut(String inOrOut) {
		this.inOrOut = inOrOut;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
