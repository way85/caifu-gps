package com.caifugps.model;

import java.util.Date;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Oct 16 2016 Entity Budget
 */

@Entity
@Table(name = "budget")
public class Budget {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "budgetid", nullable = false)
	private Integer budgetid;

	@NotNull
	@Column(name = "asofdate")
	private Date asofdate;

	@Column(name = "food")
	private BigDecimal food;

	@Column(name = "traffic")
	private BigDecimal traffic;

	@Column(name = "dress")
	private BigDecimal dress;

	@Column(name = "housing")
	private BigDecimal housing;

	@Column(name = "entertainment")
	private BigDecimal entertainment;

	@Column(name = "health")
	private BigDecimal health;

	@Column(name = "edu")
	private BigDecimal edu;

	@Column(name = "web")
	private BigDecimal web;

	@Column(name = "misc")
	private BigDecimal misc;

	@Column(name = "id", nullable = false)
	private Integer id;

	public Budget() {
	}

	public Budget(Integer budgetid, BigDecimal food, BigDecimal traffic, BigDecimal dress, BigDecimal housing,
			BigDecimal entertainment, BigDecimal health, BigDecimal edu, BigDecimal web, BigDecimal misc, Integer id) {
		this.budgetid = budgetid;
		this.food = food;
		this.traffic = traffic;
		this.dress = dress;
		this.housing = housing;
		this.entertainment = entertainment;
		this.health = health;
		this.edu = edu;
		this.web = web;
		this.misc = misc;
		this.id = id;
	}

	public Integer getBudgetid() {
		return budgetid;
	}

	public void setBudgetid(Integer budgetid) {
		this.budgetid = budgetid;
	}

	public Date getAsofdate() {
		return asofdate;
	}

	public void setAsofdate(Date asofdate) {
		this.asofdate = asofdate;
	}

	public BigDecimal getFood() {
		return food;
	}

	public void setFood(BigDecimal food) {
		this.food = food;
	}

	public BigDecimal getTraffic() {
		return traffic;
	}

	public void setTraffic(BigDecimal traffic) {
		this.traffic = traffic;
	}

	public BigDecimal getDress() {
		return dress;
	}

	public void setDress(BigDecimal dress) {
		this.dress = dress;
	}

	public BigDecimal getHousing() {
		return housing;
	}

	public void setHousing(BigDecimal housing) {
		this.housing = housing;
	}

	public BigDecimal getEntertainment() {
		return entertainment;
	}

	public void setEntertainment(BigDecimal entertainment) {
		this.entertainment = entertainment;
	}

	public BigDecimal getHealth() {
		return health;
	}

	public void setHealth(BigDecimal health) {
		this.health = health;
	}

	public BigDecimal getEdu() {
		return edu;
	}

	public void setEdu(BigDecimal edu) {
		this.edu = edu;
	}

	public BigDecimal getWeb() {
		return web;
	}

	public void setWeb(BigDecimal web) {
		this.web = web;
	}

	public BigDecimal getMisc() {
		return misc;
	}

	public void setMisc(BigDecimal misc) {
		this.misc = misc;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
