package com.caifugps.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "finalpro")
public class FinalPro {

	@Id
	@Column(name = "fpid", nullable = false)
    private Integer fpid;
	
	@Column(name = "id")
    private int id;
	
	@Column(name = "version")
    private char version;
   
	@Column(name = "personalscore")
    private String personalscore;
	
	@Column(name = "riskscore")
    private String riskscore;
	
	@Column(name = "finalscore")
    private String finalscore;
	
	@Column(name = "asofdate")
    private Date asofdate;
	
	@Column(name = "financialscore")
    private String financialscore;

   public FinalPro() {
   }

   public FinalPro(int id, char version,  String personalscore, String riskscore, String finalscore, Date asofdate, String financialscore) {
      this.id = id;
      this.version = version;      
      this.personalscore = personalscore;
      this.riskscore = riskscore;
      this.finalscore = finalscore;
      this.asofdate = asofdate;
      this.financialscore = financialscore;
   }
  
   public Integer getFpid() {
       return this.fpid;
   }
   
   public void setFpid(Integer fpid) {
       this.fpid = fpid;
   }
   public int getId() {
       return this.id;
   }
   
   public void setId(int id) {
       this.id = id;
   }
   public char getVersion() {
       return this.version;
   }
   
   public void setVersion(char version) {
       this.version = version;
   }
 
   public String getPersonalscore() {
       return this.personalscore;
   }
   
   public void setPersonalscore(String personalscore) {
       this.personalscore = personalscore;
   }
   public String getRiskscore() {
       return this.riskscore;
   }
   
   public void setRiskscore(String riskscore) {
       this.riskscore = riskscore;
   }
   public String getFinalscore() {
       return this.finalscore;
   }
   
   public void setFinalscore(String finalscore) {
       this.finalscore = finalscore;
   }
   public Date getAsofdate() {
       return this.asofdate;
   }
   
   public void setAsofdate(Date asofdate) {
       this.asofdate = asofdate;
   }
   public String getFinancialscore() {
       return this.financialscore;
   }
   
   public void setFinancialscore(String financialscore) {
       this.financialscore = financialscore;
   }




}


