package com.caifugps.model;

import java.util.Date;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * 
 */

@Entity
@Table(name = "outcomeAcc")
public class OutcomeAcc implements Account{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "iadid", nullable = false)
	private long iadid;
	
	@Column(name = "id", nullable = false)
	private Integer id;
	
	@Column(name = "type")
	int type ;
	
	@Column(name = "date")
	String date ;
	
	@Column(name = "month")
	String month ;
	
	@Column(name = "year")
	String year ;
	
	@NotNull
	@Column(name = "amount")
	BigDecimal amount ;	

	@Column(name = "tag1")
	String tag1 ;

	@Column(name = "tag2")
	String tag2 ;

	@Column(name = "tag3")
	String tag3 ;

	@Column(name = "tag4")
	String tag4 ;

	@Column(name = "tag5")
	String tag5 ;
	
	@Column(name = "description")
	String description;

	public OutcomeAcc(){
	
	}

	public OutcomeAcc(Integer id,int type, String date, String month, String year, BigDecimal amount,  String tag1, String tag2, String tag3,
			String tag4, String tag5) {
		super();
		this.id = id;
		this.type=type;
		this.date=date;
		this.month=month;
		this.year=year;
		
		this.amount = amount;
		this.tag1 = tag1;
		this.tag2 = tag2;
		this.tag3 = tag3;
		this.tag4 = tag4;
		this.tag5 = tag5;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	

	public String getDate() {
		return date;
	}

	public long getIadid() {
		return iadid;
	}

	public Integer getId() {
		return id;
	}

	public String getMonth() {
		return month;
	}

	public String getTag1() {
		return tag1;
	}
	
	public String getTag2() {
		return tag2;
	}

	public String getTag3() {
		return tag3;
	}
	
	public String getTag4() {
		return tag4;
	}

	public String getTag5() {
		return tag5;
	}
	
	public int getType() {
		return type;
	}

	public String getYear() {
		return year;
	}
	
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	

	public void setDate(String date) {
		this.date =date;
	}

	public void setIadid(long iadid) {
		this.iadid = iadid;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public void setTag1(String tag1) {
		this.tag1 = tag1;
	}

	public void setTag2(String tag2) {
		this.tag2 = tag2;
	}

	public void setTag3(String tag3) {
		this.tag3 = tag3;
	}

	public void setTag4(String tag4) {
		this.tag4 = tag4;
	}
	
	public void setTag5(String tag5) {
		this.tag5 = tag5;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public void setYear(String year) {
		this.year = year;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	@Override
	public String toString() {
		return "OutcomeAcc [id=" + id + ", type=" + type + ",date=" + date + ",month=" + month + ",year=" + year + ", amount=" + amount + ", tag1=" + tag1 + ", tag2="
				+ tag2 + ", tag3=" + tag3 + ", tag4=" + tag4 + ", tag5=" + tag5 + "]";
	}

	

	
	
}
