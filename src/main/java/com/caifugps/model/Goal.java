package com.caifugps.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "goal")
public class Goal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "goalid", nullable = false)
	private Integer goalid;

	@Column(name = "id")
	private int id;

	@Column(name = "goaltype")
	private String goaltype;

	@Column(name = "timeframe")
	private String timeframe;
	

	@Column(name = "asofdate")
	private Date asofdate;

	@Column(name = "expectedamount")
	private String expectedamount;

	@Column(name = "startamount")
	private String startamount;

	@Column(name = "incrementalamount")
	private String incrementalamount;

	@Column(name = "goalstatus")
	private String goalstatus;	

	public Goal() {
	}

	public Goal(String goaltype, String timeframe, String expectedamount, String startamount,
			String incrementalamount) {
		this.goaltype = goaltype;
		this.timeframe = timeframe;
		this.expectedamount = expectedamount;
		this.startamount = startamount;
		this.incrementalamount = incrementalamount;

	}

	public Goal(int id, String goaltype, String timeframe, String expectedamount,
			String startamount, String incrementalamount, String goalstatus, Date asofdate) {
		this.id = id;
		this.goaltype = goaltype;
		this.timeframe = timeframe;		
		this.expectedamount = expectedamount;
		this.startamount = startamount;
		this.incrementalamount = incrementalamount;
		this.goalstatus = goalstatus;
		this.asofdate = asofdate;
	}

	public Integer getGoalid() {
		return this.goalid;
	}

	public void setGoalid(Integer goalid) {
		this.goalid = goalid;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGoaltype() {
		return this.goaltype;
	}

	public void setGoaltype(String goaltype) {
		this.goaltype = goaltype;
	}

	public String getTimeframe() {
		return this.timeframe;
	}

	public void setTimeframe(String timeframe) {
		this.timeframe = timeframe;
	}

	public Date getAsofdate() {
		return this.asofdate;
	}

	public void setAsofdate(Date asofdate) {
		this.asofdate = asofdate;
	}

	public String getExpectedamount() {
		return this.expectedamount;
	}

	public void setExpectedamount(String expectedamount) {
		this.expectedamount = expectedamount;
	}

	public String getStartamount() {
		return this.startamount;
	}

	public void setStartamount(String startamount) {
		this.startamount = startamount;
	}

	public String getIncrementalamount() {
		return this.incrementalamount;
	}

	public void setIncrementalamount(String incrementalamount) {
		this.incrementalamount = incrementalamount;
	}

	public String getGoalstatus() {
		return this.goalstatus;
	}

	public void setGoalstatus(String goalstatus) {
		this.goalstatus = goalstatus;
	}

	

}
