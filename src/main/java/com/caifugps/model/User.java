package com.caifugps.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * 
 */

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@NotNull
	@Column(name = "displayname")
	String displayname = "";

	@NotNull
	@Column(name = "password")
	String password = "";
	
	@NotNull
	@Column(name = "userregistered")
	Date userregistered;

	public Date getUserregistered() {
		return userregistered;
	}

	public void setUserregistered(Date userregistered) {
		this.userregistered = userregistered;
	}

	public User() {
	}	

	public User(String displayname, String password) {
		this.displayname = displayname;
		this.password = password;		
	}
	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDisplayname() {
		return this.displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

}
