package com.caifugps.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "scoreprofile")
public class ScorePro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "spid", nullable = false)
	Integer spid;
	
	@NotNull
	@Column(name = "id")
    int id;
	
	
	@Column(name = "currentage")
     private  String currentage="";
	
	@Column(name = "retirementage")
     private  String retirementage="";
     
	@Column(name = "investmentknowledge")
     private char investmentknowledge;
	
	@Column(name = "investmentexperience")
     private char investmentexperience;
	
	@Column(name = "selfscore")
     private char selfscore;
	
	@Column(name = "bailout")
     private char bailout;
	
	@Column(name = "emergencyfund")
     private char emergencyfund;
	
	@Column(name = "annualincome")
     private char annualincome;
	
	@Column(name = "essentialexpense")
     private char essentialexpense;
	
	@Column(name = "overallsituation")
     private char overallsituation;
	
	@Column(name = "asofdate")
     private Date asofdate;

    public ScorePro() {
    }

    public ScorePro(int id, String currentage, String retirementage, char investmentknowledge, char investmentexperience, char selfscore, char bailout, char emergencyfund, char annualincome, char essentialexpense, char overallsituation,Date asofdate) {
       this.id = id;
       this.currentage = currentage;
       this.retirementage = retirementage;
       this.investmentknowledge = investmentknowledge;
       this.investmentexperience = investmentexperience;
       this.selfscore = selfscore;
       this.bailout = bailout;
       this.emergencyfund = emergencyfund;
       this.annualincome = annualincome;
       this.essentialexpense = essentialexpense;
       this.overallsituation = overallsituation;
       this.asofdate = asofdate;
    }
   
    public Integer getSpid() {
        return this.spid;
    }
    
    public void setSpid(Integer spid) {
        this.spid = spid;
    }
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getCurrentage() {
        return this.currentage;
    }
    
    public void setCurrentage(String currentage) {
        this.currentage = currentage;
    }
    public String getRetirementage() {
        return this.retirementage;
    }
    
    public void setRetirementage(String retirementage) {
        this.retirementage = retirementage;
    }
    public char getInvestmentknowledge() {
        return this.investmentknowledge;
    }
    
    public void setInvestmentknowledge(char investmentknowledge) {
        this.investmentknowledge = investmentknowledge;
    }
    public char getInvestmentexperience() {
        return this.investmentexperience;
    }
    
    public void setInvestmentexperience(char investmentexperience) {
        this.investmentexperience = investmentexperience;
    }
    public char getSelfscore() {
        return this.selfscore;
    }
    
    public void setSelfscore(char selfscore) {
        this.selfscore = selfscore;
    }
    public char getBailout() {
        return this.bailout;
    }
    
    public void setBailout(char bailout) {
        this.bailout = bailout;
    }
    public char getEmergencyfund() {
        return this.emergencyfund;
    }
    
    public void setEmergencyfund(char emergencyfund) {
        this.emergencyfund = emergencyfund;
    }
    public char getAnnualincome() {
        return this.annualincome;
    }
    
    public void setAnnualincome(char annualincome) {
        this.annualincome = annualincome;
    }
    public char getEssentialexpense() {
        return this.essentialexpense;
    }
    
    public void setEssentialexpense(char essentialexpense) {
        this.essentialexpense = essentialexpense;
    }
    public char getOverallsituation() {
        return this.overallsituation;
    }
    
    public void setOverallsituation(char overallsituation) {
        this.overallsituation = overallsituation;
    }

    public Date getAsofdate() {
        return this.asofdate;
    }
    
    public void setAsofdate(Date asofdate) {
        this.asofdate = asofdate;
    }

}