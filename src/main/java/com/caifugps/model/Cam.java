package com.caifugps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import java.util.Date;


@Entity
@Table(name = "cam")
public class Cam  implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "camid", nullable = false)
     private Integer camid;
     
	@Column(name = "id")
     private int id;
     
	@Column(name = "stockportion")
     private String stockportion="0";
     
	@Column(name = "bondsportion")
     private String bondsportion="0";
     
	@Column(name = "fixincomeportion")
     private String fixincomeportion="0";
     
	@Column(name = "shorttermportion")
     private String shorttermportion="0";
     
	@Column(name = "otherportion")
     private String otherportion="0";
     
	@Column(name = "camtype")
     private String camtype="0";
     
	@Column(name = "asofdate")
     private Date asofdate;

    public Cam() {
    }

    public Cam(int id, String stockportion, String bondsportion, String fixincomeportion, String shorttermportion, String otherportion, String camtype, Date asofdate) {
       this.id = id;
       this.stockportion = stockportion;
       this.bondsportion = bondsportion;
       this.fixincomeportion = fixincomeportion;
       this.shorttermportion = shorttermportion;
       this.otherportion = otherportion;
       this.camtype = camtype;
       this.asofdate = asofdate;
    }
   
    public Integer getCamid() {
        return this.camid;
    }
    
    public void setCamid(Integer camid) {
        this.camid = camid;
    }
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getStockportion() {
        return this.stockportion;
    }
    
    public void setStockportion(String stockportion) {
        this.stockportion = stockportion;
    }
    public String getBondsportion() {
        return this.bondsportion;
    }
    
    public void setBondsportion(String bondsportion) {
        this.bondsportion = bondsportion;
    }
    public String getFixincomeportion() {
        return this.fixincomeportion;
    }
    
    public void setFixincomeportion(String fixincomeportion) {
        this.fixincomeportion = fixincomeportion;
    }
    public String getShorttermportion() {
        return this.shorttermportion;
    }
    
    public void setShorttermportion(String shorttermportion) {
        this.shorttermportion = shorttermportion;
    }
    public String getOtherportion() {
        return this.otherportion;
    }
    
    public void setOtherportion(String otherportion) {
        this.otherportion = otherportion;
    }
    public String getCamtype() {
        return this.camtype;
    }
    
    public void setCamtype(String camtype) {
        this.camtype = camtype;
    }
    public Date getAsofdate() {
        return this.asofdate;
    }
    
    public void setAsofdate(Date asofdate) {
        this.asofdate = asofdate;
    }




}


