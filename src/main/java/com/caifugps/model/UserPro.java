package com.caifugps.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * 
 */

@Entity
@Table(name = "userprofile")
public class UserPro {

	@Id
	@Column(name = "id")
	private Integer id;

	@NotNull
	@Column(name = "displayname")
	String displayname = "";


	@Column(name = "email")
	String email = "";

	
	@Column(name = "age")
	int age = 0;

	
	@Column(name = "gender")
	char gender = 'm';

	@Column(name = "city")
	String city = "";

	@Column(name = "career")
	String career = "";
	
	@Column(name = "userregistered")
	Date userregistered;
	
	@Column(name = "usertags")
	String usertags;
	
	@Column(name = "userlevel")
	String userlevel;

	public UserPro() {
	}

	public UserPro(int id,String displayname) {
		
		this.id=id;
		this.displayname = displayname;
		
	}
	

	public UserPro(String displayname,  String email, char age, char gender, String city, String career,String userlevel,String usertags, Date userregistered) {
		this.displayname = displayname;
		this.email = email;
		this.age = age;
		this.gender = gender;
		this.city = city;
		this.career = career;
		this.userregistered = userregistered;
		this.userlevel = userlevel;
		this.usertags = usertags;
	}
	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDisplayname() {
		return this.displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	

	public String getUsertags() {
		return usertags;
	}



	public void setUsertags(String usertags) {
		this.usertags = usertags;
	}



	public String getUserlevel() {
		return userlevel;
	}



	public void setUserlevel(String userlevel) {
		this.userlevel = userlevel;
	}



	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(char age) {
		this.age = age;
	}

	public char getGender() {
		return this.gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCareer() {
		return this.career;
	}

	public void setCareer(String career) {
		this.career = career;
	}
	
	public Date getUserregistered() {
        return this.userregistered;
    }
    
    public void setUserregistered(Date userregistered) {
        this.userregistered = userregistered;
    }

}
