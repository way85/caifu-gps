package com.caifugps.model;
// Generated 2016-3-26 11:51:28 by Hibernate Tools 4.3.1


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "portifolioperformance")
public class PorPer  implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ppid")
     private Integer ppid;
     
	@Column(name = "id")
     private int id;
     
	@Column(name = "totalamount")
     private String totalamount="0";
     
	@Column(name = "stockintrumentid")
     private String stockintrumentid="0";
     
	@Column(name = "stockamount")
     private String stockamount="0";
     
	@Column(name = "bondsintrumentid")
     private String bondsintrumentid="0";
     
	@Column(name = "bondsamount")
     private String bondsamount="0";
     
	@Column(name = "fixincomeintrumentid")
     private String fixincomeintrumentid="0";
     
	@Column(name = "fixincomeamount")
     private String fixincomeamount="0";
     
	@Column(name = "shorttermintrumentid")
     private String shorttermintrumentid="0";  
     
	@Column(name = "shorttermamount")
     private String shorttermamount="0";
     
     @Column(name = "otherintrumentid") 
     private String otherintrumentid="0";
     
     @Column(name = "otheramount") 
     private String otheramount="0";
     
     @Column(name = "ppdate")
     private Date ppdate;

    public PorPer() {
    }

    public PorPer(int id, String totalamount, String stockintrumentid, String stockamount, String bondsintrumentid, String bondsamount, String fixincomeintrumentid, String fixincomeamount, String shorttermintrumentid, String shorttermamount, String otherintrumentid, String otheramount, Date ppdate) {
       this.id = id;
       this.totalamount = totalamount;
       this.stockintrumentid = stockintrumentid;
       this.stockamount = stockamount;
       this.bondsintrumentid = bondsintrumentid;
       this.bondsamount = bondsamount;
       this.fixincomeintrumentid = fixincomeintrumentid;
       this.fixincomeamount = fixincomeamount;
       this.shorttermintrumentid = shorttermintrumentid;
       this.shorttermamount = shorttermamount;
       this.otherintrumentid = otherintrumentid;
       this.otheramount = otheramount;
       this.ppdate = ppdate;
    }
    
    public PorPer(String stockintrumentid, String bondsintrumentid  ) {
        this.stockintrumentid = stockintrumentid;
        this.bondsintrumentid = bondsintrumentid;
     }
   
    public Integer getPpid() {
        return this.ppid;
    }
    
    public void setPpid(Integer ppid) {
        this.ppid = ppid;
    }
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getTotalamount() {
        return this.totalamount;
    }
    
    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }
    public String getStockintrumentid() {
        return this.stockintrumentid;
    }
    
    public void setStockintrumentid(String stockintrumentid) {
        this.stockintrumentid = stockintrumentid;
    }
    public String getStockamount() {
        return this.stockamount;
    }
    
    public void setStockamount(String stockamount) {
        this.stockamount = stockamount;
    }
    public String getBondsintrumentid() {
        return this.bondsintrumentid;
    }
    
    public void setBondsintrumentid(String bondsintrumentid) {
        this.bondsintrumentid = bondsintrumentid;
    }
    public String getBondsamount() {
        return this.bondsamount;
    }
    
    public void setBondsamount(String bondsamount) {
        this.bondsamount = bondsamount;
    }
    public String getFixincomeintrumentid() {
        return this.fixincomeintrumentid;
    }
    
    public void setFixincomeintrumentid(String fixincomeintrumentid) {
        this.fixincomeintrumentid = fixincomeintrumentid;
    }
    public String getFixincomeamount() {
        return this.fixincomeamount;
    }
    
    public void setFixincomeamount(String fixincomeamount) {
        this.fixincomeamount = fixincomeamount;
    }
    public String getShorttermintrumentid() {
        return this.shorttermintrumentid;
    }
    
    public void setShorttermintrumentid(String shorttermintrumentid) {
        this.shorttermintrumentid = shorttermintrumentid;
    }
    public String getShorttermamount() {
        return this.shorttermamount;
    }
    
    public void setShorttermamount(String shorttermamount) {
        this.shorttermamount = shorttermamount;
    }
    public String getOtherintrumentid() {
        return this.otherintrumentid;
    }
    
    public void setOtherintrumentid(String otherintrumentid) {
        this.otherintrumentid = otherintrumentid;
    }
    public String getOtheramount() {
        return this.otheramount;
    }
    
    public void setOtheramount(String otheramount) {
        this.otheramount = otheramount;
    }
    public Date getPpdate() {
        return this.ppdate;
    }
    
    public void setPpdate(Date ppdate) {
        this.ppdate = ppdate;
    }




}


