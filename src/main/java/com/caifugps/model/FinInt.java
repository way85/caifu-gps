package com.caifugps.model;
// Generated 2016-3-26 11:51:28 by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import java.util.Date;

@Entity
@Table(name = "financialintrument")
public class FinInt implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "financialintrumentid", nullable = false)
	private String financialintrumentid;

	@Column(name = "financialintrumentname")
	private String financialintrumentname;

	@Column(name = "financialintrumenttype")
	private String financialintrumenttype;

	@Column(name = "financialintrumentnetvalue")
	private String financialintrumentnetvalue;

	@Column(name = "financialintrumentrate")
	private String financialintrumentrate;

	@Column(name = "riskrate")
	private String riskrate;

	@Column(name = "yeartonowreturn")
	private String yeartonowreturn;

	@Column(name = "asofdate")
	private Date asofdate;

	public FinInt() {
	}

	public FinInt(String financialintrumentid, String financialintrumentname, String financialintrumenttype,
			String financialintrumentnetvalue, String financialintrumentrate, String riskrate, String yeartonowreturn,
			Date asofdate) {
		this.financialintrumentid = financialintrumentid;
		this.financialintrumentname = financialintrumentname;
		this.financialintrumenttype = financialintrumenttype;
		this.financialintrumentnetvalue = financialintrumentnetvalue;
		this.financialintrumentrate = financialintrumentrate;
		this.riskrate = riskrate;
		this.yeartonowreturn = yeartonowreturn;
		this.asofdate = asofdate;
	}

	public String getFinancialintrumentid() {
		return this.financialintrumentid;
	}

	public void setFinancialintrumentid(String financialintrumentid) {
		this.financialintrumentid = financialintrumentid;
	}

	public String getFinancialintrumentname() {
		return this.financialintrumentname;
	}

	public void setFinancialintrumentname(String financialintrumentname) {
		this.financialintrumentname = financialintrumentname;
	}

	public String getFinancialintrumenttype() {
		return this.financialintrumenttype;
	}

	public void setFinancialintrumenttype(String financialintrumenttype) {
		this.financialintrumenttype = financialintrumenttype;
	}

	public String getFinancialintrumentnetvalue() {
		return this.financialintrumentnetvalue;
	}

	public void setFinancialintrumentnetvalue(String financialintrumentnetvalue) {
		this.financialintrumentnetvalue = financialintrumentnetvalue;
	}

	public String getFinancialintrumentrate() {
		return this.financialintrumentrate;
	}

	public void setFinancialintrumentrate(String financialintrumentrate) {
		this.financialintrumentrate = financialintrumentrate;
	}

	public String getRiskrate() {
		return this.riskrate;
	}

	public void setRiskrate(String riskrate) {
		this.riskrate = riskrate;
	}

	public String getYeartonowreturn() {
		return this.yeartonowreturn;
	}

	public void setYeartonowreturn(String yeartonowreturn) {
		this.yeartonowreturn = yeartonowreturn;
	}

	public Date getAsofdate() {
		return this.asofdate;
	}

	public void setAsofdate(Date asofdate) {
		this.asofdate = asofdate;
	}

}
